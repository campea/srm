package com.srm.services.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.srm.model.dto.QuotationDTO;
import com.srm.repository.entity.Quotation;

/**
 * Mapper pour les devis 
 */
@Mapper(uses = {ProductMapper.class, ContactMapper.class})
public interface QuotationMapper {
	
	Quotation quotationDTOToQuotation(QuotationDTO quotationDTO);
	
	QuotationDTO quotationToQuotationDTO(Quotation quotation);
	
	List<Quotation> quotationDTOsToQuotations(Iterable<QuotationDTO> quotations);
	
	List<QuotationDTO> quotationsToQuotationDTOs(Iterable<Quotation> quotations);

}
