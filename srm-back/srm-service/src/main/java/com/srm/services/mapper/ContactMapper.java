package com.srm.services.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.srm.model.dto.ContactDTO;
import com.srm.repository.entity.Contact;

/**
 * Mapper pour les contacts
 */
@Mapper
public interface ContactMapper {

	ContactMapper INSTANCE = Mappers.getMapper(ContactMapper.class);

	Contact contactDTOToContact(ContactDTO contactDTO);

	ContactDTO contactToContactDTO(Contact contact);

	List<Contact> contactDTOsToContacts(Iterable<ContactDTO> contacts);

	List<ContactDTO> contactsToContactDTOs(Iterable<Contact> contacts);

}
