package com.srm.services.interfaces;

import java.util.List;

import com.srm.model.dto.QuotationDTO;

/**
 * Interface du service des devis
 */
public interface QuotationService {
    
    /**
     * Récupère les données d'un devis
     * 
     * @param id un id de devis
     * @return un devis
     */
    QuotationDTO getQuotation(Long id);

    /**
     * Enregistre un devis
     * 
     * @param quotation un devis
     * @return le devis enregistré
     */
	QuotationDTO saveQuotation(QuotationDTO quotation);

	/**
	 * Récupère les devis
	 * 
	 * @return une liste de devis
	 */
	List<QuotationDTO> getQuotations();

	/**
	 * Supprime le devis dont l'id est passé en paramètre
	 * 
	 * @param id L'id du devis à supprimer
	 */
	void deleteQuotation(Long id);
}
