package com.srm.services.interfaces;

import com.srm.model.dto.UserDTO;

/**
 * Interface du service des utilisateurs
 */
public interface UserService {

	/**
	 * Récupère un utilisateur
	 * 
	 * @param email l'email d'un utilisateur
	 * @return un utilisateur
	 */
	UserDTO getUser(String email);

	/**
	 * Enregistre un nouvel utilisateur
	 * 
	 * @param user un utilisateur
	 * @return un utilisateur enregitré
	 */
	UserDTO saveUser(UserDTO user);
}
