package com.srm.services.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.srm.model.dto.ProductDTO;
import com.srm.repository.entity.Product;

/**
 * Mapper pour les produit 
 */
@Mapper
public interface ProductMapper {

	ProductMapper INSTANCE = Mappers.getMapper( ProductMapper.class );
	
	Product productDTOToProduct(ProductDTO productDTO);
	
	ProductDTO productToProductDTO(Product product);

}
