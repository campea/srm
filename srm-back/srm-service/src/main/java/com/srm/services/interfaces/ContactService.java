package com.srm.services.interfaces;

import java.util.List;

import com.srm.model.dto.ContactDTO;

/**
 * Interface du service des contacts
 */
public interface ContactService {

	/**
	 * Récupère les contacts
	 * 
	 * @return une liste de contacts
	 */
	List<ContactDTO> getContacts();

	/**
	 * Récupère les données d'un contact
	 * 
	 * @param id l'id du contact à récupérer 
	 * @return un contact
	 */
	ContactDTO getContact(Long id);

	/**
	 * Supprime le contact dont l'id est passée en paramètre
	 * 
	 * @param id l'id du contact à supprimer
	 */
	void deleteContact(Long id);

    /**
     * Enregistre un contact
     * 
     * @param contact un contact
     * @return le contact enregistré
     */
	ContactDTO saveContact(ContactDTO contact);
}
