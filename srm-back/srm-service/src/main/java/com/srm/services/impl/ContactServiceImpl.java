package com.srm.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.srm.model.dto.ContactDTO;
import com.srm.repository.entity.Contact;
import com.srm.repository.interfaces.ContactRepository;
import com.srm.services.enums.ServiceNames;
import com.srm.services.interfaces.ContactService;
import com.srm.services.mapper.ContactMapper;

/**
 * Service des contacts
 */
@Service(ServiceNames.CONTACT_SERVICE)
public class ContactServiceImpl implements ContactService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContactServiceImpl.class);

    @Autowired
    private ContactRepository contactRepository;
    
    @Autowired
    private ContactMapper contactMapper;
    
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ContactDTO> getContacts() {
		LOGGER.debug("Récupération des contacts");
		return contactMapper.contactsToContactDTOs(contactRepository.findAll());
	}
	
    /**
     * {@inheritDoc}
     */
	@Override
	public ContactDTO getContact(Long id) {
		LOGGER.debug("Récupération du contact : " + id);
		return contactMapper.contactToContactDTO(contactRepository.findOne(id));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteContact(Long id) {
		LOGGER.debug("Suppression du contact : " + id);
		contactRepository.delete(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContactDTO saveContact(ContactDTO contact) {
		LOGGER.debug("Enregistrement d'un nouveau contact");
		Contact savedContact = contactRepository.save(contactMapper.contactDTOToContact(contact));
		return contactMapper.contactToContactDTO(savedContact);
	}
}
