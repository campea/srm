package com.srm.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.srm.model.dto.EventDTO;
import com.srm.repository.entity.Event;
import com.srm.repository.interfaces.EventRepository;
import com.srm.services.enums.ServiceNames;
import com.srm.services.interfaces.ScheduleService;
import com.srm.services.mapper.EventMapper;

/**
 * Service du planning
 */
@Service(ServiceNames.SCHEDULE_SERVICE)
public class ScheduleServiceImpl implements ScheduleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleServiceImpl.class);

    @Autowired
    private EventRepository eventRepository;
    
    @Autowired
    private EventMapper eventMapper;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<EventDTO> saveScheduleEvents(List<EventDTO> events) {
		LOGGER.debug("Enregistrement du planning");
		Iterable<Event> savedEvents = eventRepository.save(eventMapper.eventDTOsToEvents(events));
		return eventMapper.eventsToEventDTOs(savedEvents);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<EventDTO> getScheduleEvents() {
		LOGGER.debug("Récupération des événements du planning");
		return eventMapper.eventsToEventDTOs(eventRepository.findAll());
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteScheduleEvent(Long id) {
		LOGGER.debug("Suppression de l'evenement :" + id);
		eventRepository.delete(id);
	}
}
