package com.srm.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.srm.databus.producer.Producer;
import com.srm.model.dto.QuotationDTO;
import com.srm.repository.entity.Quotation;
import com.srm.repository.interfaces.QuotationRepository;
import com.srm.services.enums.ServiceNames;
import com.srm.services.interfaces.QuotationService;
import com.srm.services.mapper.QuotationMapper;

/**
 * Service des devis
 */
@Service(ServiceNames.QUOTATION_SERVICE)
public class QuotationServiceImpl implements QuotationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuotationServiceImpl.class);

    @Autowired
    private QuotationRepository quotationRepository;
    
    @Autowired
    private QuotationMapper quotationMapper;
    
	@Autowired
	private Producer producer;
    
    /**
     * {@inheritDoc}
     */
	@Override
	public QuotationDTO getQuotation(Long id) {
		LOGGER.debug("Récupération du devis : " + id);
		return quotationMapper.quotationToQuotationDTO(quotationRepository.findOne(id));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public QuotationDTO saveQuotation(QuotationDTO quotation) {
		LOGGER.debug("Enregistrement d'un nouveau devis");
		Quotation savedQuotation = quotationRepository.save(quotationMapper.quotationDTOToQuotation(quotation));
		QuotationDTO quotationDTO = quotationMapper.quotationToQuotationDTO(savedQuotation);
		producer.send(quotationDTO);
		return quotationDTO;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<QuotationDTO> getQuotations() {
		LOGGER.debug("Récupération des devis");
		return quotationMapper.quotationsToQuotationDTOs(quotationRepository.findAll());
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteQuotation(Long id) {
		LOGGER.debug("Suppression du devis : " + id);
		quotationRepository.delete(id);
	}
}
