package com.srm.services.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.srm.model.dto.EventDTO;
import com.srm.repository.entity.Event;

/**
 * Mapper pour le planning
 */
@Mapper
public interface EventMapper {

	EventMapper INSTANCE = Mappers.getMapper(EventMapper.class);

	Event eventDTOToEvent(EventDTO eventDTO);

	EventDTO eventToEventDTO(Event event);

	List<Event> eventDTOsToEvents(Iterable<EventDTO> events);

	List<EventDTO> eventsToEventDTOs(Iterable<Event> events);

}
