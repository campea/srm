package com.srm.services.interfaces;

import java.util.List;

import com.srm.model.dto.EventDTO;

/**
 * Interface du service du planning
 */
public interface ScheduleService {

	/**
	 * Enregistre les evenements du planning
	 * 
	 * @param events Une liste d'evenements à sauvegarder
	 * @return Une liste d'evenements enregistrés
	 */
	List<EventDTO> saveScheduleEvents(List<EventDTO> events);
	
	/**
	 * Récupère la liste des évenements
	 * 
	 * @return Une liste d'evenements
	 */
	List<EventDTO> getScheduleEvents();

	/**
	 * Suppression de l'evenement dont l'id est passé en paramètre
	 * 
	 * @param id l'id de l'evenement à supprimer
	 */
	void deleteScheduleEvent(Long id);
}
