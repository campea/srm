package com.srm.services.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.srm.model.dto.UserDTO;
import com.srm.repository.entity.User;

/**
 * Mapper pour les utilisateurs
 */
@Mapper(uses = {ContactMapper.class})
public interface UserMapper {
	
	User userDTOToUser(UserDTO userDTO);
	
	UserDTO userToUserDTO(User user);
	
	List<User> userDTOsToUsers(Iterable<UserDTO> users);
	
	List<UserDTO> usersToUserDTOs(Iterable<User> users);

}
