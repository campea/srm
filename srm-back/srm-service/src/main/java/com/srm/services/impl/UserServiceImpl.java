package com.srm.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.srm.model.dto.UserDTO;
import com.srm.repository.entity.User;
import com.srm.repository.interfaces.UserRepository;
import com.srm.services.enums.ServiceNames;
import com.srm.services.interfaces.UserService;
import com.srm.services.mapper.UserMapper;

/**
 * Service des utilisateurs
 */
@Service(ServiceNames.USER_SERVICE)
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserMapper userMapper;
    
    /**
     * {@inheritDoc}
     */
	@Override
	public UserDTO getUser(String email) {
		LOGGER.debug("Récupération de l'utilisateur ayant pour email : " + email);
		User user = userRepository.findByEmail(email);
		if (user == null) {
			return null;
		}
		return userMapper.userToUserDTO(user);
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public UserDTO saveUser(UserDTO user) {
		LOGGER.debug("Enregistrement d'un nouvel utilisateur");
		User savedUser = userRepository.save(userMapper.userDTOToUser(user));
		return userMapper.userToUserDTO(savedUser);
	}
}
