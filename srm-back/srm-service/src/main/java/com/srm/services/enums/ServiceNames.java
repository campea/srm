package com.srm.services.enums;

/**
 * Classe définissant la liste des services
 */
public class ServiceNames {
    
    public static final String QUOTATION_SERVICE = "quotationService";

    public static final String CONTACT_SERVICE = "contactService";
    
    public static final String SCHEDULE_SERVICE = "scheduleService";
    
    public static final String USER_SERVICE = "userService";
    
    /**
     * Constructeur
     */
    private ServiceNames() {
    }
}
