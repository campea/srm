package com.srm.databus.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.srm.model.dto.QuotationDTO;

@Service
public class Producer {

    private static final Logger LOG = LoggerFactory.getLogger(Producer.class);

    @Autowired
    private KafkaTemplate<String, QuotationDTO> kafkaTemplate;

    @Value("${spring.kafka.topic}")
    private String topic;

    public void send(QuotationDTO quotation){
        LOG.info("sending message='{}' to topic='{}'", quotation.toString(), topic);
        kafkaTemplate.send(topic, quotation);
    }
}