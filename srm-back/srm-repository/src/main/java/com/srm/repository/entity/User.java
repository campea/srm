package com.srm.repository.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.srm.repository.enums.TableNames;

/**
 * Entité qui représente un utilisateur
 */
@Entity
@Table(name = TableNames.USER_TABLE)
public class User {

	/**
	 * Id de l'utilisateur
	 */
	@Id
	@Column(name = "ID_USER", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * Prénom
	 */
	@Column(name = "USR_FIRSTNAME", nullable = false)
	private String firstName;

	/**
	 * Nom
	 */
	@Column(name = "USR_LASTNAME", nullable = false)
	private String lastName;

	/**
	 * E-mail
	 */
	@Column(name = "USR_EMAIL", nullable = false)
	private String email;

	/**
	 * Mot de passe
	 */
	@Column(name = "USR_PASSWORD", nullable = false)
	private String password;

	/**
	 * Liste de numéro de téléphone
	 */
	@Column(name = "USR_PHONE", nullable = false)
	private String phone;

	/**
	 * Une liste de contact
	 */
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "REL_USER_CONTACT", joinColumns = {
			@JoinColumn(name = "RUC_USER_ID", referencedColumnName = "ID_USER") }, inverseJoinColumns = {
					@JoinColumn(name = "RUC_CONTACT_ID", referencedColumnName = "ID_CONTACT") })
	private List<Contact> contacts;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

}
