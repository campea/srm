package com.srm.repository.enums;

/**
 * Classe définissant la liste des repositories
 */
public class RepositoryNames {
    
    public static final String QUOTATION_REPOSITORY = "quotationRepository";
    
    public static final String PRODUCT_REPOSITORY = "productRepository";
    
    public static final String USER_REPOSITORY = "userRepository";
    
    public static final String COMPANY_REPOSITORY = "companyRepository";
    
    public static final String CONTACT_REPOSITORY = "contactRepository";

    public static final String EVENT_REPOSITORY = "eventRepository";
    
    /**
     * Constructeur
     */
    private RepositoryNames() {
    }
}
