package com.srm.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.srm.repository.enums.TableNames;

/**
 * Entité qui représente une société
 */
@Entity
@Table(name = TableNames.COMPANY_TABLE)
public class Company {

	/**
	 * Id de la société
	 */
	@Id
	@Column(name = "ID_COMPANY", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * Nom
	 */
	@Column(name = "CPY_NAME", nullable = false)
	private String name;

	/**
	 * Code NAF
	 */
	@Column(name = "CPY_NAF_CODE", nullable = false)
	private String naf;

	/**
	 * Numéro SIREN
	 */
	@Column(name = "CPY_SIREN", nullable = false)
	private String siren;

	/**
	 * Numéro SIRET
	 */
	@Column(name = "CPY_SIRET", nullable = false)
	private String siret;

	/**
	 * Numéro de rue
	 */
	@Column(name = "CPY_STREET_NUMBER", nullable = false)
	private Long streetNumber;

	/**
	 * Rue
	 */
	@Column(name = "CPY_STREET", nullable = false)
	private String street;

	/**
	 * Code postale
	 */
	@Column(name = "CPY_ZIP_CODE", nullable = false)
	private String zipCode;

	/**
	 * Ville
	 */
	@Column(name = "CPY_CITY", nullable = false)
	private String city;

	/**
	 * Pays
	 */
	@Column(name = "CPY_COUNTRY", nullable = false)
	private String country;

	/**
	 * Numéro de téléphone
	 */
	@Column(name = "CPY_PHONE", nullable = false)
	private String phone;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNaf() {
		return naf;
	}

	public void setNaf(String naf) {
		this.naf = naf;
	}

	public String getSiren() {
		return siren;
	}

	public void setSiren(String siren) {
		this.siren = siren;
	}

	public String getSiret() {
		return siret;
	}

	public void setSiret(String siret) {
		this.siret = siret;
	}

	public Long getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(Long streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
