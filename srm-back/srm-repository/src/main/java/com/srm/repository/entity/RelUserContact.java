package com.srm.repository.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.srm.repository.enums.TableNames;

/**
 * Entité qui représente la table de relation utilisateur/contact
 */
@Entity
@Table(name = TableNames.REL_USER_CONTACT_TABLE)
public class RelUserContact {

	@EmbeddedId
	private RelUserContactPK id;

	public RelUserContactPK getId() {
		return id;
	}

	public void setId(RelUserContactPK id) {
		this.id = id;
	}

}
