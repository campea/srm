package com.srm.repository.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.srm.repository.enums.TableNames;

/**
 * Entité qui représente la table de relation devis/produit
 */
@Entity
@Table(name = TableNames.REL_QUOTATION_PRODUCT_TABLE)
public class RelQuotationProduct {

	@EmbeddedId
	private RelQuotationProductPK id;

	public RelQuotationProductPK getId() {
		return id;
	}

	public void setId(RelQuotationProductPK id) {
		this.id = id;
	}

}
