package com.srm.repository.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RelUserContactPK implements Serializable {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id du devis
	 */
	@Column(name = "RUC_USER_ID", nullable = false)
	private Long userId;

	/**
	 * Id du produit
	 */
	@Column(name = "RUC_CONTACT_ID", nullable = false)
	private Long contactId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getContactId() {
		return contactId;
	}

	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

}