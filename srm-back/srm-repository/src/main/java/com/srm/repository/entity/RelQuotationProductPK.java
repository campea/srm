package com.srm.repository.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RelQuotationProductPK implements Serializable {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id du devis
	 */
	@Column(name = "RQP_QUOTATION_ID", nullable = false)
	private Long quotationId;

	/**
	 * Id du produit
	 */
	@Column(name = "RQP_PRODUCT_ID", nullable = false)
	private Long productId;

	public Long getQuotationId() {
		return quotationId;
	}

	public void setQuotationId(Long quotationId) {
		this.quotationId = quotationId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

}