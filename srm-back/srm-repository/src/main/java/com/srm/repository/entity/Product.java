package com.srm.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.srm.repository.enums.TableNames;

/**
 * Entité qui représente un produit
 */
@Entity
@Table(name = TableNames.PRODUCT_TABLE)
public class Product {

	/**
	 * Id du produit
	 */
	@Id
	@Column(name = "ID_PRODUCT", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * Référence produit
	 */
	@Column(name = "PDT_REFERENCE", nullable = false)
	private String reference;

	/**
	 * Nom du produit
	 */
	@Column(name = "PDT_NAME", nullable = false)
	private String name;

	/**
	 * Prix HT
	 */
	@Column(name = "PDT_HT_PRICE", nullable = false)
	private Double priceHT;

	/**
	 * Quantité
	 */
	@Column(name = "PDT_QUANTITY", nullable = false)
	private Integer quantity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPriceHT() {
		return priceHT;
	}

	public void setPriceHT(Double priceHT) {
		this.priceHT = priceHT;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

}
