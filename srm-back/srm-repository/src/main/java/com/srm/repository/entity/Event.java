package com.srm.repository.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.srm.repository.enums.TableNames;

/**
 * Entité qui représente un evenement du planning
 */
@Entity
@Table(name = TableNames.EVENT_TABLE)
public class Event {
	/**
	 * Id du devis
	 */
	@Id
	@Column(name = "ID_EVENT", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * Date de début de l'évenement
	 */
	@Column(name = "EVT_START_DATE", nullable = false)
	private Date start;

	/**
	 * Date de fin de l'évenement
	 */
	@Column(name = "EVT_END_DATE", nullable = false)
	private Date end;

	/**
	 * Nom de l'évenement
	 */
	@Column(name = "EVT_TITLE", nullable = false)
	private String title;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the start
	 */
	public Date getStart() {
		return start;
	}

	/**
	 * @param start
	 *            the start to set
	 */
	public void setStart(Date start) {
		this.start = start;
	}

	/**
	 * @return the end
	 */
	public Date getEnd() {
		return end;
	}

	/**
	 * @param end
	 *            the end to set
	 */
	public void setEnd(Date end) {
		this.end = end;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

}