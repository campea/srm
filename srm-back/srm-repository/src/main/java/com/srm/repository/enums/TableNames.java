package com.srm.repository.enums;

/**
 * Classe définissant la liste des tables
 */
public class TableNames {

	public static final String QUOTATION_TABLE = "QUOTATION";

	public static final String PRODUCT_TABLE = "PRODUCT";

	public static final String USER_TABLE = "USERS";

	public static final String CONTACT_TABLE = "CONTACT";

	public static final String COMPANY_TABLE = "COMPANY";

	public static final String REL_QUOTATION_PRODUCT_TABLE = "REL_QUOTATION_PRODUCT";

	public static final String REL_USER_CONTACT_TABLE = "REL_USER_CONTACT";
	
	public static final String EVENT_TABLE = "EVENT";

	/**
	 * Constructeur
	 */
	private TableNames() {
	}
}
