package com.srm.repository.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.srm.repository.enums.TableNames;

/**
 * Entité qui représente un devis
 */
@Entity
@Table(name = TableNames.QUOTATION_TABLE)
public class Quotation {

	/**
	 * Id du devis
	 */
	@Id
	@Column(name = "ID_QUOTATION", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * Produits
	 */
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "REL_QUOTATION_PRODUCT", joinColumns = {
			@JoinColumn(name = "RQP_QUOTATION_ID", referencedColumnName = "ID_QUOTATION") }, inverseJoinColumns = {
					@JoinColumn(name = "RQP_PRODUCT_ID", referencedColumnName = "ID_PRODUCT") })
	private List<Product> products;

	/**
	 * Prix HT
	 */
	@Column(name = "QTA_HT_PRICE", nullable = false)
	private Double priceHT;

	/**
	 * Prix TTC
	 */
	@Column(name = "QTA_TTC_PRICE", nullable = false)
	private Double priceTTC;

	/**
	 * Pourcentage de taxe
	 */
	@Column(name = "QTA_TAX", nullable = false)
	private Double tax;

	/**
	 * Pourcentage de remise
	 */
	@Column(name = "QTA_DISCOUNT", nullable = true)
	private Double discount;

	/**
	 * Contact
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "QTA_CONTACT_ID", referencedColumnName = "ID_CONTACT", nullable = false)
	private Contact contact;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the priceHT
	 */
	public Double getPriceHT() {
		return priceHT;
	}

	/**
	 * @param priceHT
	 *            the priceHT to set
	 */
	public void setPriceHT(Double priceHT) {
		this.priceHT = priceHT;
	}

	/**
	 * @return the priceTTC
	 */
	public Double getPriceTTC() {
		return priceTTC;
	}

	/**
	 * @param priceTTC
	 *            the priceTTC to set
	 */
	public void setPriceTTC(Double priceTTC) {
		this.priceTTC = priceTTC;
	}

	/**
	 * @return the tax
	 */
	public Double getTax() {
		return tax;
	}

	/**
	 * @param tax
	 *            the tax to set
	 */
	public void setTax(Double tax) {
		this.tax = tax;
	}

	/**
	 * @return the discount
	 */
	public Double getDiscount() {
		return discount;
	}

	/**
	 * @param discount
	 *            the discount to set
	 */
	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	/**
	 * @return the products
	 */
	public List<Product> getProducts() {
		return products;
	}

	/**
	 * @param products
	 *            the products to set
	 */
	public void setProducts(List<Product> products) {
		this.products = products;
	}

	/**
	 * @return the contact
	 */
	public Contact getContact() {
		return contact;
	}

	/**
	 * @param contact
	 *            the contact to set
	 */
	public void setContact(Contact contact) {
		this.contact = contact;
	}

}
