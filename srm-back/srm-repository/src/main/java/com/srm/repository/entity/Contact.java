package com.srm.repository.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.srm.common.enums.ContactType;
import com.srm.repository.enums.TableNames;

/**
 * Entité qui représente un contact
 */
@Entity
@Table(name = TableNames.CONTACT_TABLE)
public class Contact {

	/**
	 * Id du client/prospect
	 */
	@Id
	@Column(name = "ID_CONTACT", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * Un prénom
	 */
	@Column(name = "CNT_FIRSTNAME", nullable = false)
	private String firstName;

	/**
	 * Un Nom
	 */
	@Column(name = "CNT_LASTNAME", nullable = false)
	private String lastName;

	/**
	 * Une société
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "CNT_COMPANY_ID", referencedColumnName = "ID_COMPANY", nullable = false)
	private Company company;

	/**
	 * Un type de contact
	 */
	@Column(name = "CNT_TYPE", nullable = false)
	@Enumerated(EnumType.STRING)
	private ContactType type;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company
	 *            the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the type
	 */
	public ContactType getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(ContactType type) {
		this.type = type;
	}

}