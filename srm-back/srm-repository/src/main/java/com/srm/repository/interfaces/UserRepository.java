package com.srm.repository.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.srm.repository.entity.User;
import com.srm.repository.enums.RepositoryNames;

/**
 * Repository pour les utilisateurs
 */
@Repository(RepositoryNames.USER_REPOSITORY)
public interface UserRepository extends CrudRepository<User, Long> {
	User findByEmail(String email);
}
