package com.srm.repository.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.srm.repository.entity.Product;
import com.srm.repository.enums.RepositoryNames;

/**
 * Repository pour les produits
 */
@Repository(RepositoryNames.PRODUCT_REPOSITORY)
public interface ProductRepository extends CrudRepository<Product, Long> {
}
