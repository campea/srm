package com.srm.repository.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.srm.repository.entity.Event;
import com.srm.repository.enums.RepositoryNames;

/**
 * Repository pour les evenement
 */
@Repository(RepositoryNames.EVENT_REPOSITORY)
public interface EventRepository extends CrudRepository<Event, Long> {
}
