package com.srm.repository.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.srm.repository.entity.Contact;
import com.srm.repository.enums.RepositoryNames;

/**
 * Repository pour les contacts
 */
@Repository(RepositoryNames.CONTACT_REPOSITORY)
public interface ContactRepository extends CrudRepository<Contact, Long> {
}
