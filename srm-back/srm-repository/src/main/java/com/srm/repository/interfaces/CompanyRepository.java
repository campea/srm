package com.srm.repository.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.srm.repository.entity.Company;
import com.srm.repository.enums.RepositoryNames;

/**
 * Repository pour les sociétés
 */
@Repository(RepositoryNames.COMPANY_REPOSITORY)
public interface CompanyRepository extends CrudRepository<Company, Long> {
}
