package com.srm.model.dto;

import java.util.List;

import com.srm.common.enums.ContactType;

/**
 * DTO qui représente un contact
 */
public class ContactDTO {

	/**
	 * Id du client/prospect
	 */
	private Long id;
	
	/**
	 * Un prénom
	 */
	private String firstName;

	/**
	 * Un Nom
	 */
	private String lastName;

	/**
	 * Une société
	 */
	private CompanyDTO company;

	/**
	 * Une liste de devis
	 */
	private List<QuotationDTO> quotations;

	/**
	 * Un type de contact
	 */
	private ContactType type;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the company
	 */
	public CompanyDTO getCompany() {
		return company;
	}

	/**
	 * @param company
	 *            the company to set
	 */
	public void setCompany(CompanyDTO company) {
		this.company = company;
	}

	/**
	 * @return the quotations
	 */
	public List<QuotationDTO> getQuotations() {
		return quotations;
	}

	/**
	 * @param quotations
	 *            the quotations to set
	 */
	public void setQuotations(List<QuotationDTO> quotations) {
		this.quotations = quotations;
	}

	/**
	 * @return the type
	 */
	public ContactType getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(ContactType type) {
		this.type = type;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
