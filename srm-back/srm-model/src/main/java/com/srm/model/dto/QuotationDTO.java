package com.srm.model.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO qui représente un devis
 */
public class QuotationDTO {

	/**
	 * Id du devis
	 */
	private Long id;

	/**
	 * Produit
	 */
	private List<ProductDTO> products = new ArrayList<>();

	/**
	 * Prix HT
	 */
	private Double priceHT;

	/**
	 * Prix TTC
	 */
	private Double priceTTC;

	/**
	 * Pourcentage de taxe
	 */
	private Double tax;

	/**
	 * Pourcentage de remise
	 */
	private Double discount;
	
	/**
	 * Contact
	 */
	private ContactDTO contact;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the products
	 */
	public List<ProductDTO> getProducts() {
		return products;
	}

	/**
	 * @param products
	 *            the products to set
	 */
	public void setProducts(List<ProductDTO> products) {
		this.products = products;
	}

	/**
	 * @return the priceHT
	 */
	public Double getPriceHT() {
		return priceHT;
	}

	/**
	 * @param priceHT
	 *            the priceHT to set
	 */
	public void setPriceHT(Double priceHT) {
		this.priceHT = priceHT;
	}

	/**
	 * @return the priceTTC
	 */
	public Double getPriceTTC() {
		return priceTTC;
	}

	/**
	 * @param priceTTC
	 *            the priceTTC to set
	 */
	public void setPriceTTC(Double priceTTC) {
		this.priceTTC = priceTTC;
	}

	/**
	 * @return the tax
	 */
	public Double getTax() {
		return tax;
	}

	/**
	 * @param tax
	 *            the tax to set
	 */
	public void setTax(Double tax) {
		this.tax = tax;
	}

	/**
	 * @return the discount
	 */
	public Double getDiscount() {
		return discount;
	}

	/**
	 * @param discount
	 *            the discount to set
	 */
	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	/**
	 * @return the contact
	 */
	public ContactDTO getContact() {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(ContactDTO contact) {
		this.contact = contact;
	}

}
