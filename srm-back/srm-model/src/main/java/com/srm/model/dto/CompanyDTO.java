package com.srm.model.dto;

/**
 * DTO qui représente une société
 */
public class CompanyDTO {

	/**
	 * Id de la société
	 */
	private Long id;

	/**
	 * Nom
	 */
	private String name;

	/**
	 * Code NAF
	 */
	private String naf;

	/**
	 * Numéro SIREN
	 */
	private String siren;

	/**
	 * Numéro SIRET
	 */
	private String siret;

	/**
	 * Numéro de rue
	 */
	private Long streetNumber;

	/**
	 * Rue
	 */
	private String street;

	/**
	 * Code postale
	 */
	private String zipCode;

	/**
	 * Ville
	 */
	private String city;

	/**
	 * Pays
	 */
	private String country;

	/**
	 * Numéro de téléphone
	 */
	private String phone;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the naf
	 */
	public String getNaf() {
		return naf;
	}

	/**
	 * @param naf
	 *            the naf to set
	 */
	public void setNaf(String naf) {
		this.naf = naf;
	}

	/**
	 * @return the siren
	 */
	public String getSiren() {
		return siren;
	}

	/**
	 * @param siren
	 *            the siren to set
	 */
	public void setSiren(String siren) {
		this.siren = siren;
	}

	/**
	 * @return the siret
	 */
	public String getSiret() {
		return siret;
	}

	/**
	 * @param siret
	 *            the siret to set
	 */
	public void setSiret(String siret) {
		this.siret = siret;
	}

	/**
	 * @return the streetNumber
	 */
	public Long getStreetNumber() {
		return streetNumber;
	}

	/**
	 * @param streetNumber
	 *            the streetNumber to set
	 */
	public void setStreetNumber(Long streetNumber) {
		this.streetNumber = streetNumber;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

}
