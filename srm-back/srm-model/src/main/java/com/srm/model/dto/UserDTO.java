package com.srm.model.dto;

import java.util.List;

/**
 * DTO qui représente un utilisateur
 */
public class UserDTO {

	/**
	 * Id de l'utilisateur
	 */
	private Long id;

	/**
	 * Prénom
	 */
	private String firstName;

	/**
	 * Nom
	 */
	private String lastName;

	/**
	 * E-mail
	 */
	private String email;

	/**
	 * Mot de passe
	 */
	private String password;

	/**
	 * Numéro de téléphone
	 */
	private String phone;

	/**
	 * Une liste de contact
	 */
	private List<ContactDTO> contacts;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the contacts
	 */
	public List<ContactDTO> getContacts() {
		return contacts;
	}

	/**
	 * @param contacts
	 *            the contacts to set
	 */
	public void setContacts(List<ContactDTO> contacts) {
		this.contacts = contacts;
	}

}
