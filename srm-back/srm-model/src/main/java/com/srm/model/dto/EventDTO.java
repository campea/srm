package com.srm.model.dto;

import java.util.Date;

/**
 * DTO qui représente un evenement du planning
 */
public class EventDTO {

	/**
	 * Id du devis
	 */
	private Long id;
	
	/**
	 * Date de début de l'évenement
	 */
	private Date start;
	
	/**
	 * Date de fin de l'évenement
	 */
	private Date end;
	
	/**
	 * Nom de l'évenement
	 */
	private String title;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the start
	 */
	public Date getStart() {
		return start;
	}

	/**
	 * @param start the start to set
	 */
	public void setStart(Date start) {
		this.start = start;
	}

	/**
	 * @return the end
	 */
	public Date getEnd() {
		return end;
	}

	/**
	 * @param end the end to set
	 */
	public void setEnd(Date end) {
		this.end = end;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
