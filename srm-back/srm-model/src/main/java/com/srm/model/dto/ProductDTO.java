package com.srm.model.dto;

/**
 * DTO qui représente un produit
 */
public class ProductDTO {

	/**
	 * Id du produit
	 */
	private Long id;

	/**
	 * Référence produit
	 */
	private String reference;

	/**
	 * Nom du produit
	 */
	private String name;

	/**
	 * Prix HT
	 */
	private Double priceHT;

	/**
	 * Quantité
	 */
	private Integer quantity;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference
	 *            the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the priceHT
	 */
	public Double getPriceHT() {
		return priceHT;
	}

	/**
	 * @param priceHT
	 *            the priceHT to set
	 */
	public void setPriceHT(Double priceHT) {
		this.priceHT = priceHT;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

}
