package com.srm.common.enums;

/**
 * Enum des types de contact
 */
public enum ContactType {

	PROSPECT,
	CLIENT;
}
