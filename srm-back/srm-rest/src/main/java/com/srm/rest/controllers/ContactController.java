package com.srm.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.srm.model.dto.ContactDTO;
import com.srm.rest.enums.Endpoints;
import com.srm.services.interfaces.ContactService;

/**
 * Contrôleur des contacts
 */
@RestController
@RequestMapping(Endpoints.CONTACT)
public class ContactController {

	@Autowired
	private ContactService contactService;

	@RequestMapping(method = RequestMethod.GET)
	public List<ContactDTO> getContacts() {
		return contactService.getContacts();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteContact(@PathVariable("id") Long id) {
		contactService.deleteContact(id);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ContactDTO getContact(@PathVariable("id") Long id) {
		return contactService.getContact(id);
	}
	
	@RequestMapping(value="/save", method = RequestMethod.POST)
	public ContactDTO saveContact(@RequestBody ContactDTO contact) {
		return contactService.saveContact(contact);
	}
}
