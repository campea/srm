package com.srm.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.srm.model.dto.QuotationDTO;
import com.srm.rest.enums.Endpoints;
import com.srm.services.interfaces.QuotationService;

/**
 * Contrôleur des devis
 */
@RestController
@RequestMapping(Endpoints.QUOTATION)
public class QuotationController {

	@Autowired
	private QuotationService quotationService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public QuotationDTO getQuotation(@PathVariable("id") Long id) {
		return quotationService.getQuotation(id);
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<QuotationDTO> getQuotations() {
		return quotationService.getQuotations();
	}

	@RequestMapping(value="/save", method = RequestMethod.POST)
	public QuotationDTO saveQuotation(@RequestBody QuotationDTO quotation) {
		return quotationService.saveQuotation(quotation);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteQuotation(@PathVariable("id") Long id) {
		quotationService.deleteQuotation(id);
	}
}
