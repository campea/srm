package com.srm.rest.enums;

/**
 * Classe définissant le chemin des packages
 */
public class Packages {

    /**
     * Constructeur
     */
    private Packages() {
    }
    
    public static final String MODEL = "com.srm.model";
    
    public static final String REPOSITORIES = "com.srm.repository";

    public static final String REST = "com.srm.rest";

    public static final String SERVICES = "com.srm.services";
    
    public static final String COMMON = "com.srm.common";
    
    public static final String DATABUS = "com.srm.databus";
}
