package com.srm.rest.enums;

public final class Endpoints {

    public static final String QUOTATION = "/quotations";
    
    public static final String CONTACT = "/contacts";
    
    public static final String SCHEDULE = "/schedule";
    
    public static final String USER = "/users";
	
}
