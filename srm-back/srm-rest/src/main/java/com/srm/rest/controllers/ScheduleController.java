package com.srm.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.srm.model.dto.EventDTO;
import com.srm.rest.enums.Endpoints;
import com.srm.services.interfaces.ScheduleService;

/**
 * Contrôleur du planning
 */
@RestController
@RequestMapping(Endpoints.SCHEDULE)
public class ScheduleController {

	@Autowired
	private ScheduleService scheduleService;

	@RequestMapping(value="/save", method = RequestMethod.POST)
	public List<EventDTO> saveQuotation(@RequestBody List<EventDTO> events) {
		return scheduleService.saveScheduleEvents(events);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public List<EventDTO> getScheduleEvents() {
		return scheduleService.getScheduleEvents();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteSchdeduleEvent(@PathVariable("id") Long id) {
		scheduleService.deleteScheduleEvent(id);
	}
}
