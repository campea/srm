package com.srm.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.srm.rest.enums.Packages;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = { Packages.MODEL, Packages.SERVICES, Packages.REST, Packages.REPOSITORIES, Packages.COMMON, Packages.DATABUS})
@EntityScan({Packages.REPOSITORIES})
@EnableJpaRepositories(Packages.REPOSITORIES)
public class SrmApplication {

	public static void main(String[] args) {
		SpringApplication.run(SrmApplication.class, args);
	}
}
