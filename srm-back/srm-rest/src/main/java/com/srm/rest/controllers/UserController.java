package com.srm.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.srm.model.dto.UserDTO;
import com.srm.rest.enums.Endpoints;
import com.srm.services.interfaces.UserService;

/**
 * Contrôleur des utilisateurs
 */
@RestController
@RequestMapping(Endpoints.USER)
public class UserController {

	@Autowired
	private UserService userService;
		
	@RequestMapping(method = RequestMethod.POST)
	public UserDTO getUser(@RequestBody UserDTO user) {
		return userService.getUser(user.getEmail());
	}
	
	@RequestMapping(value="/save", method = RequestMethod.POST)
	public UserDTO saveUser(@RequestBody UserDTO user) {
		return userService.saveUser(user);
	}
}
