// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const urlConstants = {
  quotations: 'quotations/',
  contacts: 'contacts/',
  schedule: 'schedule/',
  save: 'save/',
  users: 'users/',
  user: 'user/',
  authentication: 'authentication/',
  logout: 'logout/',
  login: '/login',
  colors: {
    red: {
      primary: '#ad2121',
      secondary: '#FAE3E3'
    },
    blue: {
      primary: '#1e90ff',
      secondary: '#D1E8FF'
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA'
    }
  }

};
