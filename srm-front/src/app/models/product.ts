export class Product {
  id: number;
  reference: string;
  name: string;
  priceHT: number;
  quantity: number;

  constructor() {
    this.id = null;
    this.reference = null;
    this.name = null;
    this.priceHT = null;
    this.quantity = null;
  }
}
