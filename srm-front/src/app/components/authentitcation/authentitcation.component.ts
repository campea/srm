import { urlConstants } from './../../shared/constants';
import { AuthenticationService } from './../../services/authentication/authentication.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';

@Component({
  selector: 'app-authentitcation',
  templateUrl: './authentitcation.component.html',
  styleUrls: ['./authentitcation.component.css']
})
export class AuthentitcationComponent {

  private user = new User();

  constructor(private authenticationService: AuthenticationService, private http: HttpClient, private router: Router) {
    sessionStorage.setItem("authenticated", "false");
  }

  login() {
    this.authenticationService.authenticate(this.user, 
      () => {
        if (sessionStorage.getItem("authenticated") == "true") {
          this.router.navigateByUrl('/quotation/quotation-list');
        } else {
          this.router.navigateByUrl('/login');
        }
    });
  }

}
