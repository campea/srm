import { urlConstants } from './../../../shared/constants';
import { AuthenticationService } from './../../../services/authentication/authentication.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactService } from './../../../services/contact/contact.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Contact } from './../../../models/contact';
import { Component, OnInit } from '@angular/core';
import { Quotation } from '../../../models/quotation';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.css']
})
export class EditContactComponent implements OnInit {

  private contact: Contact = new Contact();

  private form: FormGroup;

  private id: string;

  constructor(private contactService: ContactService, private route: ActivatedRoute, private fb: FormBuilder, private snackBar: MatSnackBar, private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit() {
    if (sessionStorage.getItem("authenticated") == "false") {
      this.router.navigateByUrl(urlConstants.login);
    }

    this.form = this.fb.group({
      id: [{ value: null, disabled: true }],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      type: ['', [Validators.required]],
      quotations: new Array<Quotation>(),
      company: this.fb.group({
        id: [{ value: null, disabled: true }],
        name: ['', [Validators.required]],
        naf: ['', [Validators.required]],
        siren: ['', [Validators.required]],
        siret: ['', [Validators.required]],
        streetNumber: ['', [Validators.pattern("^[0-9]*$"), Validators.required, Validators.min(1)]],
        street: ['', [Validators.required]],
        zipCode: ['', [Validators.required]],
        city: ['', [Validators.required]],
        country: ['', [Validators.required]],
        phone: ['', [Validators.required]]
      })
    });

    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != null) {
      this.getContact(this.id);
    }
  }

  getContact(id: string) {
    this.contactService.getContact(this.id).subscribe(
      data => {
        this.contact = data;
        this.buildForm(this.contact);
        this.form.setValue(this.contact);
      },
      err => console.log(err)
    );
  }

  buildForm(contact: Contact) {
    this.form = this.fb.group({
      id: [{ value: this.contact.id, disabled: true }],
      firstName: [this.contact.firstName, [Validators.required]],
      lastName: [this.contact.lastName, [Validators.required]],
      type: [this.contact.company.type, [Validators.required]],
      quotations: this.contact.quotations,
      company: this.fb.group({
        id: [{ value: this.contact.company.id, disabled: true }],
        name: [this.contact.company.name, [Validators.required]],
        naf: [this.contact.company.naf, [Validators.required]],
        siren: [this.contact.company.siren, [Validators.required]],
        siret: [this.contact.company.siret, [Validators.required]],
        streetNumber: [this.contact.company.streetNumber, [Validators.pattern("^[0-9]*$"), Validators.required, Validators.min(1)]],
        street: [this.contact.company.street, [Validators.required]],
        zipCode: [this.contact.company.zipCode, [Validators.required]],
        city: [this.contact.company.city, [Validators.required]],
        country: [this.contact.company.country, [Validators.required]],
        phone: [this.contact.company.phone, [Validators.required]]
      })
    });
  }

  save(contact: Contact, isValid: boolean) {
    if (isValid) {
      this.contactService.saveContact(contact).subscribe(
        () => this.openSnackBar('Le contact a été enregistré avec succès', 'center', 'bottom')
      );
    }
  }

  openSnackBar(message: string, horizontal: MatSnackBarHorizontalPosition, vertical: MatSnackBarVerticalPosition) {
    this.snackBar.open(message, null, {
      duration: 2000,
      horizontalPosition: horizontal,
      verticalPosition: vertical
    });
  }

}
