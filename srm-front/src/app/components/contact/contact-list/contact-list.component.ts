import { urlConstants } from './../../../shared/constants';
import { AuthenticationService } from './../../../services/authentication/authentication.service';
import { Router } from '@angular/router';
import { ContactService } from './../../../services/contact/contact.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Contact } from './../../../models/contact';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {

  private displayedColumns = ['firstName', 'lastName', 'company', 'type', 'edit', 'delete'];
  
  private contactDatasource: MatTableDataSource<Contact> = new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private contactService: ContactService, private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit() {
    if (sessionStorage.getItem("authenticated") == "false") {
      this.router.navigateByUrl(urlConstants.login);
    }

    this.getContacts();
    this.contactDatasource.paginator = this.paginator;
  }

  deleteContact(id: string) {
    this.contactService.deleteContact(id).subscribe(
      data => { this.getContacts() },
      err => console.error(err)
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.contactDatasource.filter = filterValue;
  }

  getContacts() {
    this.contactService.getContacts().subscribe(
      data => { this.contactDatasource.data = data },
      err => console.error(err)
    );
  }
}