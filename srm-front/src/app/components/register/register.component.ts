import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { AuthenticationService } from './../../services/authentication/authentication.service';
import { User } from './../../models/user';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  private user = new User();

  constructor(private authenticationService: AuthenticationService, private router: Router, private snackBar: MatSnackBar) {
  }

  register() {
    this.authenticationService.register(this.user).subscribe( 
      () => {
          this.openSnackBar('Vous êtes maintenant inscrit', 'center', 'bottom');
          this.router.navigateByUrl('/login');
    });
    return false;
  }

  openSnackBar(message: string, horizontal: MatSnackBarHorizontalPosition, vertical: MatSnackBarVerticalPosition) {
    this.snackBar.open(message, null, {
      duration: 2000,
      horizontalPosition: horizontal,
      verticalPosition: vertical
    });
  }

}
