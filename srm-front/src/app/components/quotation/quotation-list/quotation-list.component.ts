import { urlConstants } from './../../../shared/constants';
import { Router } from '@angular/router';
import { AuthenticationService } from './../../../services/authentication/authentication.service';
import { DataSource } from '@angular/cdk/table';
import { Observable, BehaviorSubject } from 'rxjs';
import { Quotation } from './../../../models/quotation';
import { QuotationService } from './../../../services/quotation/quotation.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatInput, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-quotation-list',
  templateUrl: './quotation-list.component.html',
  styleUrls: ['./quotation-list.component.css']
})
export class QuotationListComponent implements OnInit {

  private displayedColumns = ['id', 'priceHT', 'priceTTC', 'edit', 'delete'];
  
  private quotationDatasource: MatTableDataSource<Quotation> = new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private quotationService: QuotationService, private authenticationService: AuthenticationService, private router: Router) { }


  ngOnInit() {
    if (sessionStorage.getItem("authenticated") == "false") {
      this.router.navigateByUrl(urlConstants.login);
    }
    
    this.getQuotations();
    this.quotationDatasource.paginator = this.paginator;
  }

  deleteQuotation(id: string) {
    this.quotationService.deleteQuotation(id).subscribe(
      data => { this.getQuotations() },
      err => console.error(err)
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.quotationDatasource.filter = filterValue;
  }

  getQuotations() {
    this.quotationService.getQuotations().subscribe(
      data => { this.quotationDatasource.data = data },
      err => console.error(err)
    );
  }
}