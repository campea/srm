import { urlConstants } from './../../../shared/constants';
import { AuthenticationService } from './../../../services/authentication/authentication.service';
import { ContactService } from './../../../services/contact/contact.service';
import { Contact } from './../../../models/contact';
import { MatTableDataSource, MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { Quotation } from './../../../models/quotation';
import { QuotationService } from './../../../services/quotation/quotation.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { Product } from '../../../models/product';

@Component({
  selector: 'app-edit-quotation',
  templateUrl: './edit-quotation.component.html',
  styleUrls: ['./edit-quotation.component.css']
})
export class EditQuotationComponent implements OnInit {

  private displayedColumns = ['reference', 'name', 'quantity', 'priceHT', 'delete'];

  private quotation: Quotation = new Quotation();

  private contacts: Array<Contact>;

  private form: FormGroup;

  private id: string;

  private productDatasource: MatTableDataSource<Product> = new MatTableDataSource();

  constructor(private authenticationService: AuthenticationService, private router: Router, private quotationService: QuotationService, private contactService: ContactService, private route: ActivatedRoute, private fb: FormBuilder, private snackBar: MatSnackBar) { }

  ngOnInit() {
    if (sessionStorage.getItem("authenticated") == "false") {
      this.router.navigateByUrl(urlConstants.login);
    }    

    this.form = this.fb.group({
      id: [{ value: this.quotation.id, disabled: true }],
      priceHT: [{ value: '', disabled: true }],
      priceTTC: [{ value: '', disabled: true }],
      discount: '',
      tax: ['', [Validators.pattern("^[0-9]*$"), Validators.required, Validators.min(0), Validators.max(100)]],
      products: null,
      contact: this.quotation.contact
    });

    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    if (this.id != null) {
      this.getQuotation();
    } else {
      this.getContacts();
    }
  }

  getQuotation() {
    this.quotationService.getQuotation(this.id).subscribe(
      data => {
        this.form.setValue(data);
        this.productDatasource.data = data.products;
        this.quotation = data;
        this.getContacts();
      },
      err => console.log(err)
    );
  }

  getContacts() {
    this.contactService.getContacts().subscribe(
      data => {
        this.contacts = data;
        if (this.id) {
          let contact = this.contacts.find(contact => contact.id === this.quotation.contact.id);
          this.form.controls.contact.setValue(contact);
        }
      },
      err => console.log(err)
    );
  }

  calculatePrice(quotation: Quotation) {
    let totalPriceHT = 0;
    this.productDatasource.data.forEach(product => {
      totalPriceHT += product.priceHT * product.quantity;
    });
    this.form.patchValue({ priceHT: totalPriceHT, priceTTC: totalPriceHT * ((this.form.value.tax / 100) + 1) });
  }

  save(quotation: Quotation, isValid: boolean) {
    if (isValid && this.checkProductsValid(this.productDatasource.data)) {
      quotation.products = this.productDatasource.data;
      this.quotationService.saveQuotation(quotation).subscribe(
        data => {
          this.quotation = data;
          this.form.setValue(data);
          let contact = this.contacts.find(contact => contact.id === this.quotation.contact.id);
          this.form.controls.contact.setValue(contact);
        },
        () => {
          this.openSnackBar('Le devis a été enregistré avec succès', 'center', 'bottom')
        }
      );
    }
  }

  addProduct() {
    let data = this.productDatasource.data;
    data.push(new Product());
    this.productDatasource.data = data;
  }

  deleteProduct(index: number) {
    let data = this.productDatasource.data;
    data.splice(index, 1);
    this.productDatasource.data = data;
  }

  checkProductsValid(products: Array<Product>) {
    let isValid = true;
    products.forEach(product => {
      if (product.name == "" || product.reference == "" || product.priceHT == null || product.quantity == null) {
        isValid = false;
      }
    });
    return isValid;
  }

  openSnackBar(message: string, horizontal: MatSnackBarHorizontalPosition, vertical: MatSnackBarVerticalPosition) {
    this.snackBar.open(message, null, {
      duration: 2000,
      horizontalPosition: horizontal,
      verticalPosition: vertical
    });
  }

}
