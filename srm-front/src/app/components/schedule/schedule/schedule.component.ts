import { urlConstants } from './../../../shared/constants';
import { AuthenticationService } from './../../../services/authentication/authentication.service';
import { Router } from '@angular/router';
import { ScheduleService } from './../../../services/schedule/schedule.service';
import { MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition, MatSnackBar } from '@angular/material';
import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-schedule',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  view: string = 'month';

  locale: string = 'fr';

  viewDate: Date = new Date();

  selectedDay: number;

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  ngOnInit() {
    if (sessionStorage.getItem("authenticated") == "false") {
      this.router.navigateByUrl(urlConstants.login);
    }

    this.getScheduleEvents();
    registerLocaleData(localeFr);
  }

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = true;

  constructor(private modal: NgbModal, private scheduleService: ScheduleService, private snackBar: MatSnackBar, private authenticationService: AuthenticationService, private router: Router) { }

  getScheduleEvents() {
    this.scheduleService.getScheduleEvents().subscribe(
      data => { 
        this.events = data;
        this.events.forEach(event => {
          event.actions = this.actions;
          event.start = new Date(event.start);
          event.end = new Date(event.end);
          event.color = colors.red;
        });
        this.refresh.next(); 
      },
      err => console.error(err)
    );
  }
 
  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
      this.activeDayIsOpen = false;
    } else {
      this.activeDayIsOpen = true;
    }
    this.viewDate = date;
    this.selectedDay = date.getDay();
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.refresh.next();
  }

  addEvent(): void {
    this.events.push({
      title: 'Nouvel evenement',
      start: startOfDay(this.viewDate),
      end: endOfDay(this.viewDate),
      color: colors.red,
      draggable: true,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      }
    });
    this.refresh.next();
  }

  save() {
    this.scheduleService.saveSchedule(this.events).subscribe(
      () => this.openSnackBar('Le planning a été enregistré avec succès', 'center', 'bottom')
    );
  }

  openSnackBar(message: string, horizontal: MatSnackBarHorizontalPosition, vertical: MatSnackBarVerticalPosition) {
    this.snackBar.open(message, null, {
      duration: 2000,
      horizontalPosition: horizontal,
      verticalPosition: vertical
    });
  }
}