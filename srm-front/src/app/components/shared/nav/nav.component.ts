import { Router } from '@angular/router';
import { urlConstants } from './../../../shared/constants';
import { AuthenticationService } from './../../../services/authentication/authentication.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  ngOnInit() {
  }

  constructor(private authenticationService: AuthenticationService, private router: Router) {
  }

  logout() {
    sessionStorage.setItem("authenticated","false");
  }

  authenticated(): boolean {
    if (sessionStorage.getItem("authenticated") == "true") {
      return true;
    } else {
      return false;
    }
  }

}
