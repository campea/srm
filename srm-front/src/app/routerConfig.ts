import { RegisterComponent } from './components/register/register.component';
import { AuthentitcationComponent } from './components/authentitcation/authentitcation.component';
import { ContactListComponent } from './components/contact/contact-list/contact-list.component';
import { EditContactComponent } from './components/contact/edit-contact/edit-contact.component';
import { EditQuotationComponent } from './components/quotation/edit-quotation/edit-quotation.component';
import { Routes } from '@angular/router';
import { QuotationListComponent } from './components/quotation/quotation-list/quotation-list.component';
import { ScheduleComponent } from './components/schedule/schedule/schedule.component';

export const appRoutes: Routes = [
  {
    path: 'quotation/quotation-list',
    component: QuotationListComponent
  },
  {
    path: 'quotation/edit/:id',
    component: EditQuotationComponent
  },
  {
    path: 'quotation/edit',
    component: EditQuotationComponent
  },
  {
    path: 'contact/contact-list',
    component: ContactListComponent
  },
  {
    path: 'contact/edit/:id',
    component: EditContactComponent
  },
  {
    path: 'contact/edit',
    component: EditContactComponent
  },
  {
    path: 'planning',
    component: ScheduleComponent
  },
  {
    path: 'login',
    component: AuthentitcationComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  }
];
