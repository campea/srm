import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Srm';

  constructor(router: Router) {
    if (sessionStorage.getItem("authenticated") == "true") {
      router.navigate(['/quotation/quotation-list']);
    } else {
      router.navigate(['/login']);
    }
  }

}
