import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { urlConstants } from './../../shared/constants';
import { Injectable } from '@angular/core';
import { CalendarEvent } from 'calendar-utils';

@Injectable()
export class ScheduleService {

  constructor(private http: HttpClient) { }

  saveSchedule(schedule: Array<CalendarEvent>) {
    return this.http.post(environment.backUrl + urlConstants.schedule + urlConstants.save, schedule);
  }

  getScheduleEvents() {
    return this.http.get<Array<CalendarEvent>>(environment.backUrl + urlConstants.schedule);
  }
}
