import { urlConstants } from './../../shared/constants';
import { environment } from './../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthenticationService {

    constructor(private http: HttpClient) {
    }

    authenticate(user, callback) {

        this.http.post(environment.backUrl + urlConstants.users, user).subscribe(response => {
            if (response) {
                sessionStorage.setItem("authenticated", "true");
            } else {
                sessionStorage.setItem("authenticated", "false");
            }
            return callback && callback();
        });

    }

    register(user) {
        return this.http.post(environment.backUrl + urlConstants.users + urlConstants.save, user);
    }
}
