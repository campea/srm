import { urlConstants } from './../../shared/constants';
import { Contact } from './../../models/contact';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';

@Injectable()
export class ContactService {

  constructor(private http: HttpClient) { }

  getContacts() {
    return this.http.get<Array<Contact>>(environment.backUrl + urlConstants.contacts);
  }

  getContact(id: string) {
    return this.http.get<Contact>(environment.backUrl + urlConstants.contacts + id);
  }

  deleteContact(id: string) {
    return this.http.delete(environment.backUrl + urlConstants.contacts + id);
  }

  saveContact(contact: Contact) {
    return this.http.post(environment.backUrl + urlConstants.contacts + urlConstants.save, contact);
  }
}
