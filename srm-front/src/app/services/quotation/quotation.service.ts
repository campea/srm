import { urlConstants } from './../../shared/constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Observable } from 'rxjs';
import { Quotation } from '../../models/quotation';

@Injectable()
export class QuotationService {

  constructor(private http: HttpClient) { }

  getQuotations() {
    return this.http.get<Array<Quotation>>(environment.backUrl + urlConstants.quotations);
  }

  getQuotation(id: string) {
    return this.http.get<Quotation>(environment.backUrl + urlConstants.quotations + id);
  }

  deleteQuotation(id: string) {
    return this.http.delete(environment.backUrl + urlConstants.quotations + id);
  }

  saveQuotation(quotation: Quotation) {
    return this.http.post<Quotation>(environment.backUrl + urlConstants.quotations + urlConstants.save, quotation);
  }
}
